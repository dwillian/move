$(document).ready(function() {
    
    /*Masks of input data*/
    $("#phone").mask("(00) 0000-00009");
    $("#date").mask("00/00/0000");
    $("#cep").mask("00000-000");

    /*Using de API to recovery zipcode data from: https://webmaniabr.com*/
    correios.init( 'ODXNEvbh378IZ4QG6iLTmk04F3dAnIg6', 'BKMGRGHRLdT7WRJW2EJufbsFFwbf4ujRPYrSqUHPYb4URdBO' );
    $('#cep').correios( '#endereco', '#bairro', '#cidade', '#state');

    /*The code below is responsible for the form validation. It used glyphicon and differents colors to sinalize invalid entries.*/
    $('#contactForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite seu nome.'
                        },
                        stringLength: {
                            min: 3,
                            message: 'O seu nome deve conter mais que 3 caracteres.'
                        },
                        regexp: {
                            message: 'Digite somente letras e espaços.',
                            regexp: /^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/
                        },
                    }
                },
                phone: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'O número de telefone é necessário.'
                        },
                        regexp: {
                            message: 'Digite somente números, espaços, -, (, ), + e .',
                            regexp: /^[0-9\s\-()+\.]+$/
                        },
                        stringLength: {
                            min: 8,
                            message: 'O seu nome deve conter no mínimo 8 caracteres'
                        },
                    }
                },
                email: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite seu e-mail.'
                        },
                        emailAddress: {
                            message: 'Digite um e-mail válido.'
                        }
                    }
                },
                date: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite sua data de nascimento.'
                        },
                        regexp: {
                            message: 'Digite somente números e barras.',
                            regexp: /^[0-9\/]+$/
                        },
                        stringLength: {
                            max: 10,
                            message: 'A data com barras contém até 10 caracteres.'
                        }
                    }
                },
                address: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite seu endereço.'
                        }
                    }
                },
                neighborhood: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite seu bairro.'
                        }
                    }
                },
                state: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite seu estado.'
                        }
                    }
                },
                number: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite o número da sua residência.'
                        },
                        regexp: {
                            message: 'Digite somente números.',
                            regexp: /^[0-9]+$/
                        }
                    }
                },
                zipcode: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Precisamos saber o seu endereço pelo seu CEP.'
                        },
                        regexp: {
                            message: 'Digite somente números e traços.',
                            regexp: /^[0-9\-]+$/
                        }
                    }
                },
                city: {
                    row: '.col-xs-4',
                    validators: {
                        notEmpty: {
                            message: 'Digite a cidade.'
                        },
                    }
                }
            }
        })
});