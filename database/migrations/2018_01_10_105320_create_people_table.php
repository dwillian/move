<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('people', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 200);
			$table->string('phone', 20);
			$table->string('email')->unique();
			$table->string('date', 12);
			$table->string('zipcode', 12);
			$table->string('address', 200);
			$table->integer('number');
			$table->string('neighborhood', 200);
			$table->string('city', 200);
			$table->string('state', 200);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('people');
	}
}
