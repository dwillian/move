<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
	return view('home');
});

/*Route to PersonController which is responsible for save the data of people in Database and give them a feedback*/
Route::post('/salvar', 'PersonController@store')->name('person.store');
