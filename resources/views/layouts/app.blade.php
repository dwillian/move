<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

         <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="{{ asset('js/jquery.correios.min.js') }}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
        <script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
        <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
        <!-- Include style js-->
        <script src="{{ asset('js/style.js') }}"></script>
    </head>
    <body>
        <div id="app">
            @if(Session::has('flash_message'))
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div align="center" role="alert" class="alert {{Session::get('flash_message')['class']}}">
                                {{Session::get('flash_message')['msg']}}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @yield('content')
            @include('layouts._includes._footer')
        </div>
    </body>
</html>
