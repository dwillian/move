@extends('layouts.app')

@section('content')

<div class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="txtprincipal">
                <h1>move</h1>
                <h2>Uma vida de aventuras e desafios vale muito mais a pena!</h2>
                <br>
                <h4>Para saber mais e receber um e-book exclusivo, preencha e dê o
                    <strong><span style="color:black;font-size: large;">START</span></strong>
                    inicial pra mudar de vida.</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <form action="{{route('person.store')}}" method="POST" id="contactForm" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Nome</label>
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" class="form-control" id= "name" name="name" placeholder="Nome completo" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">*Email</label>
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                            <input type="text" class="form-control" name="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Telefone</label>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="(31) 912345-9636"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Data de Nascimento</label>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <input type="text" class="form-control" id="date" name="date" placeholder="26/04/1992"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">CEP</label>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <input type="text" class="form-control" id="cep" name="zipcode" placeholder="25698-369"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Endereço</label>
                            <input type="text" class="form-control" id="endereco" name="address" placeholder="rua, avenida, etc." />
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>Número</label>
                            <input type="text" class="form-control" id="number" name="number" placeholder="45"/>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>Complemento</label>
                            <input type="text" class="form-control" id="complement" name="complement"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>Bairro</label>
                            <input type="text" class="form-control" id="bairro" name="neighborhood" placeholder="Centro" />
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <label>Cidade</label>
                            <input type="text" class="form-control" id="cidade" name="city" placeholder="Belo Horizonte"/>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>UF</label>
                            <input type="text" class="form-control" id="state" name="state" placeholder="MG" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary btn-lg">START</button>
                        </div>
                    </div>
                <span style="color: white;">*Se já se inscreveu antes, isso é ótimo! Para sua comodidade inscrição duplicadas não serão aceitas :)</span>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="supporting">
  <div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <h2>Mova-se</h2>
          <p>Torne-se mais ativo rastreando suas corridas, passeios e caminhadas.</p>
        </div>
         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <h2>Sync</h2>
          <p>Mantenha suas atividades esportivas sincronizadas com o seu cotidiano mesmo off-line.</p>
        </div>
         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
          <h2>Competir</h2>
          <p>Saibam onde amigos estão correndo, competindo e maratonando. Crie competições!</p>
        </div>
    </div>
  </div>
</div>
    <div class="feature">
      <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h1>MOVA-SE.</h1>
            </div>
            <div class="col-xs-12 text-center">
                <h1>DESCANSE.</h1>
            </div>
            <div class="col-xs-12 text-center">
                <h1>RECUPERE-SE.</h1>
            </div>
            <div class="col-xs-12 text-center">
                <h1>MOVA-SE.</h1>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection