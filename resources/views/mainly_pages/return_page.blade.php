@extends('layouts.app')

@section('content')

<div class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="txtprincipal">
                <h1>move</h1>
                <h2>Uma vida de aventuras e desafios vale muito mais a pena!</h2>
                <br>
                <h4>Para saber mais e receber um e-book exclusivo, preencha e dê o
                    <strong><span style="color:black;font-size: large;">START</span></strong>
                    inicial pra mudar de vida.</h4>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="return_form text-center">
                	<h1>Fique ligado! Clique no botão abaixo e baixe seu <i>ebook</i> informativo.</h1>
                	<a class="btn btn-primary btn-lg" href="#contactForm">Baixar eBook</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection