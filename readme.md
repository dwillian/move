Leia o ReadME

## MOVE

   O MOVE foi a minha ideia de landing page, que seria para auxiliar na captação de leads para interessados em uma aplicação tecnológica para acompanhar os seus exercícios e aventuras do cotidiano. Inicialmente seria uma aplicação para celular, mas não falamos isso na página para instigar ainda mais a curiosidade. Oferecemos um eBook que não tive tempo para fazer, em troca da curiosidade do futuro cliente.


## Laravel

   A aplicação foi construída com a última versão do Laravel e utilizando a API indicada por e-mail para recuperar CEP. Estou entregando a aplicação um dia mais cedo do previsto pois já a terminei, porém se acharem necessário mais alguma adição, eu posso faz- la ainda em tempo. Outro ponto, eu criei a plataforma conforme interpretado por mim em relação aos requisitados, então eu posso não ter entendido bem alguma coisa que vocês esperava ver =/ ! Qualquer coisa me avisem que refaço :)

## Observações

  Eu tenho outras plataformas WEBs que construí em Laravel e um pouco mais complexas, caso desejem posso compartilhar com vocês 0/

## Agradecimento

Obrigado pela oportunidade e aguardo o feedback de vocês 0/

Douglas Lopes
dwillian.lopes@gmail.com
(31)98303-8379
