<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {
	protected $fillable = ['name', 'phone', 'email', 'date', 'zipcode', 'address', 'number', 'complement', 'neighborhood', 'city', 'state'];
}
